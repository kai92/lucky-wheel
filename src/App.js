import React, {Fragment} from 'react';
import './App.css';
import ReactFileReader from 'react-file-reader';
import {ToastContainer, toast} from "react-toastify";
import 'react-toastify/dist/ReactToastify.css';

export default class ToDoApp extends React.Component{

    constructor(props){
        super(props);
        this.state = {
            listStudent:[],
            selected : '',
            hasStop : false,
            textNoti : [
                'Xin chúc mừng  %s',
                'You are the King %s',
                'Xin Chia buồn với %s ',
                'Gió trời rất đẹp đúng không %s ? ',
                'Hôm nay nhớ đánh Đề nhé %s ',
                ' ??? %s ',
                ' %s, bạn nghĩ sao về thế giới thứ ba ?',
                ' %s, đứa ngồi bên cạnh cũng được đấy chứ ',
                ' %s, trả bài đi nào !! ',
                ' %s, có học không thì bảo ? ',
                ' %s, đừng bảo chưa thuộc bài nhé !! ',
                ' Mất Lượt !! ',
                ' Chắc chắn rồi, lần này bạn thoát :)) ',
                ' Đừng tưởng bở !! bị bắt rồi nhé %s ',
                '  %s , tớ thích màu xanh :) ',
                '  %s, đừng nói gì cả! ',
                '  ai ai cũng biết tên bạn %s ',
                '  %s, không phải lúc nào cũng có cơ hội đâu! ',
                '  %s, mạnh dạn lên :) ',
                '   %s, tớ tin ở bạn ! ',
                '  học bài rồi thì lên đây đi nào, %s ',
            ]
        };
        this.handleFiles = this.handleFiles.bind(this);
    }

    handleFiles(files){
        let self = this;
        var reader = new FileReader();
        reader.onload = function(e) {
            let result = reader.result;
            let listStudent = result.split('\n');
            if(listStudent[listStudent.length-1] === ""){
                listStudent.pop();
            }
            self.setState({listStudent});
        }
        reader.readAsText(files[0]);
    }

    setDefaultState(){
        this.setState({selected : "", hasStop : false});
    }

    removeStudent(){
        let {selected , listStudent} = this.state;
        listStudent.splice(selected, 1);
        this.setState({listStudent});
        this.setDefaultState();
    }

    runRandom(){
        this.setDefaultState();
        let {listStudent} = this.state;
        let self = this;
        let defaultLeng = listStudent.length * 3 + Math.floor(Math.random() * listStudent.length);
        let length = defaultLeng;
        let timedelay = 10;
        var i = 0;
        function myLoop () {
            setTimeout(function () {
                self.setState({selected : i});
                i++;
                length--;
                if (i < listStudent.length && length > 0) {
                    if(i === (listStudent.length -1)){
                        i = 0;
                    }
                    if(length === Math.ceil(defaultLeng*1/2)){
                        timedelay = 20;
                    }
                    if(length === Math.ceil(defaultLeng*1/4)){
                        timedelay = 50;
                    }
                    if(length === Math.ceil(defaultLeng*1/7)){
                        timedelay = 100;
                    }
                    myLoop();
                }else{
                    self.setState({hasStop : true});
                    let {selected, textNoti} = self.state;
                    let textIndex = Math.floor(Math.random() * textNoti.length);
                    let text = textNoti[textIndex];
                    text = text.replace("%s", listStudent[selected]);
                    toast.success(
                        text,
                        {
                            position: toast.POSITION.BOTTOM_CENTER,
                            className: 'wrapper-messages messages-success',
                            autoClose: 6000
                        }
                    );
                }
            }, timedelay)
        }
        myLoop();
    }

    render(){
        let {selected, hasStop} = this.state;
        return(
            <Fragment>
                <div className={"lucky-content"}>
                    <div className={"upload-button"}>
                        <ReactFileReader
                            fileTypes={'csv/*'}
                            handleFiles={this.handleFiles}>
                            <button className='btn'>{"Chọn file danh sách HS"}</button>
                        </ReactFileReader>
                        {
                            this.state.listStudent.length > 1
                                ?
                                <button className={'btn btn2'} onClick={() => this.runRandom()} >{"Bắt Đầu Quay"}</button>
                                : ""
                        }
                        {
                            this.state.listStudent.length > 1 && selected && hasStop
                                ?
                                <button className={'btn btn2'} onClick={() => this.removeStudent()} >{"Xóa HS khỏi danh sách"}</button>
                                : ""
                        }
                    </div>
                    {
                        this.state.listStudent.length > 1
                            ?
                            <div className={"menu-reports-content"}>
                                <div className={"list-menu-items"}>
                                    <ul>
                                        {
                                            this.state.listStudent.map((item, index) => {
                                                return (
                                                    <li className={selected === index ? "menu-item selected" : "menu-item"} key={index}>
                                                        <div className="menu-item-info">
                                                            <div className="menu-name">
                                                                <span dangerouslySetInnerHTML={{__html: item}}/>
                                                            </div>
                                                        </div>
                                                    </li>
                                                );
                                            })
                                        }
                                    </ul>
                                </div>
                            </div>
                            : ""
                    }
                </div>
                <ToastContainer/>
            </Fragment>
        );
    }
}